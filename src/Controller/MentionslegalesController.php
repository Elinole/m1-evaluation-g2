<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class MentionslegalesController extends AbstractController {
    /**
     * @Route("/mentionslegales", name="mentionslegales.index")
     */
    public function index():Response {
        return $this->render('mentionslegales/index.html.twig');
    }
}

?>
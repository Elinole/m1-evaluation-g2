<?php

namespace App\Controller;

use App\Repository\OeuvresRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class OeuvreController extends AbstractController {
    /**
     * @Route("/oeuvres", name="oeuvre.index")
     */
    public function index(OeuvresRepository $oeuvresRepository):Response {
        $results = $oeuvresRepository->findAll();

        return $this->render('oeuvre/index.html.twig', [
            'results' => $results
        ]);
    }

    /**
	 * @Route("/oeuvres/{id}", name="oeuvre.details")
	 */
	public function details(OeuvresRepository $oeuvresRepository, int $id):Response
	{
		// sélection d'un produit par son id
        $result = $oeuvresRepository->find($id);
        $categorie = $oeuvresRepository->find($id)->getCategorie();

		return  $this->render('oeuvre/details.html.twig', [
            'result' => $result,
            'categorie' => $categorie
		]);
    }
    
    /**
     * @Route("/oeuvres/categorie/{slug}", name="oeuvre.categorie")
     */
    public function categorie(OeuvresRepository $oeuvresRepository, string $slug):Response {
        // sélection d'un produit par son slug
		/* $results = $oeuvresRepository->findBy([
            'categorie' => $slug
        ]); */
        $results = $oeuvresRepository->findByCategorie($slug);
        $categorie = $slug;

        return $this->render('oeuvre/categorie.html.twig', [
            'results' => $results,
            'categorie' => $slug
        ]);
    }
}

?>
<?php

namespace App\Controller\Admin;

use App\Entity\Exposition;
use App\Form\ExpositionType;
use App\Repository\ExpositionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/exposition")
 */
class ExpositionController extends AbstractController {
    /**
	 * @Route("/", name="admin.exposition.index")
	 */
    public function index(ExpositionRepository $expositionRepository):Response {
        $results = $expositionRepository->findBy(
			 [], ['date' => 'DESC']
		);

        return $this->render('admin/exposition/index.html.twig', [
            'results' => $results
        ]);
    }

    /**
	 * @Route("/form", name="admin.exposition.form")
	 */
	public function form(Request $request, EntityManagerInterface $entityManager, ExpositionRepository $expositionRepository):Response
	{
		$model = new Exposition();
		$type = ExpositionType::class;
		$form = $this->createForm($type, $model);
		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()){
			// message de confirmation
			$message = "L'exposition a été ajoutée";

			// message stocké en session
            $this->addFlash('notice', $message);
            
			$model->getId() ? null : $entityManager->persist($model);
			$entityManager->flush();

			// redirection
			return $this->redirectToRoute('admin.exposition.index');
		}

		return $this->render('admin/exposition/form.html.twig', [
			'form' => $form->createView()
		]);
    }
}

?>
<?php

namespace App\Controller\Admin;

use App\Entity\Oeuvres;
use App\Form\OeuvreType;
use App\Repository\OeuvresRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/oeuvre")
 */
class OeuvreController extends AbstractController {
    /**
	 * @Route("/", name="admin.oeuvre.index")
	 */
    public function index(OeuvresRepository $oeuvresRepository):Response {
        $results = $oeuvresRepository->findAll();

        return $this->render('admin/oeuvre/index.html.twig', [
            'results' => $results
        ]);
    }

    /**
	 * @Route("/form", name="admin.oeuvre.form")
	 * @Route("/form/update/{id}", name="admin.oeuvre.form.update")
	 */
	public function form(Request $request, EntityManagerInterface $entityManager, int $id = null, OeuvresRepository $oeuvresRepository):Response
	{
		// si l'id est nul, une insertion est exécutée, sinon une modification est exécutée
		$model = $id ? $oeuvresRepository->find($id) : new Oeuvres();
		$type = OeuvreType::class;
		$form = $this->createForm($type, $model);
		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()){
			// message de confirmation
			$message = $model->getId() ? "L'oeuvre a été modifiée" : "L'oeuvre a été ajoutée";

			// message stocké en session
            $this->addFlash('notice', $message);
            
			$model->getId() ? null : $entityManager->persist($model);
			$entityManager->flush();

			// redirection
			return $this->redirectToRoute('admin.oeuvre.index');
		}

		return $this->render('admin/oeuvre/form.html.twig', [
			'form' => $form->createView()
		]);
    }
    
    /**
	 * @Route("/remove/{id}", name="admin.oeuvre.remove")
	 */
	public function remove(OeuvresRepository $oeuvresRepository, EntityManagerInterface $entityManager, int $id):Response
	{
		// autoriser la route uniquement aux super admin
		/* if(!$this->isGranted('ROLE_SUPER_ADMIN')){
			$this->addFlash('error', "Vous n'êtes pas autorisé à supprimer un produit");
			return $this->redirectToRoute('admin.oeuvre.index');
		} */

		// sélection de l'entité à supprimer
		$model = $oeuvresRepository->find($id);

		// suppression dans la table
		$entityManager->remove($model);
		$entityManager->flush();

		// suppression de l'image
		/* if(file_exists("img/product/{$model->getImage()}")){
			$fileService->remove('img/product', $model->getImage());
		} */

		// message et redirection
		$this->addFlash('notice', "L'oeuvre a été supprimée");
		return $this->redirectToRoute('admin.oeuvre.index');
	}
}

?>
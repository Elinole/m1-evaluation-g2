<?php

namespace App\Controller;

use App\Repository\ExpositionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class ExpositionController extends AbstractController {
    /**
     * @Route("/expositions", name="exposition.index")
     */
    public function index(ExpositionRepository $expositionRepository):Response {
        $results = $expositionRepository->findByDate();

        return $this->render('exposition/index.html.twig', [
            'results' => $results
        ]);
    }
}

?>
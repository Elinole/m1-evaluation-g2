<?php

namespace App\Controller;

use App\Repository\OeuvresRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class HomepageController extends AbstractController {
    /**
     * @Route("/", name="homepage.index")
     */
    public function index(OeuvresRepository $oeuvresRepository):Response {
        // sélection de 4 oeuvres
		$results = $oeuvresRepository->findBy([], [], 4);

        return $this->render('homepage/index.html.twig', [
            'results' => $results
        ]);
    }
}

?>
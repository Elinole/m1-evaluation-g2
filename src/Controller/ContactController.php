<?php

namespace App\Controller;

use App\Form\ContactType;
use App\Form\Model\ContactModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class ContactController extends AbstractController {
    /**
	 * @Route("/contact", name="contact.form")
	 */
    public function form(Request $request, Environment $twig):Response
	{
		// affichage du formulaire
		$type = ContactType::class;
		$model = new ContactModel();

		$form = $this->createForm($type, $model);

        $form->handleRequest($request);
        
		if($form->isSubmitted() && $form->isValid()){
			$this->addFlash('notice', "L'email a été envoyé");

			// redirection vers une route par son nom
			return $this->redirectToRoute('contact.form');
		}

		return $this->render('contact/form.html.twig', [
			'form' => $form->createView()
		]);
	}
}

?>
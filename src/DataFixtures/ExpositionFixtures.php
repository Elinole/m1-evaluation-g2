<?php

namespace App\DataFixtures;

use App\Entity\Exposition;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

class ExpositionFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker::create('fr_FR');

        for($i = 0; $i < 5; $i++){
            $oeuvre = new Exposition();
		    $oeuvre
                ->setName($faker->unique()->sentence(5))
                ->setDescription($faker->text)
                ->setDate($faker->dateTimeBetween('-2 years', '+2 years'))
                ->setLocation($faker->streetAddress . ', ' . $faker->city . ', ' . $faker->postcode)
            ;

		    $manager->persist($oeuvre);
	    }

        $manager->flush();
    }
}

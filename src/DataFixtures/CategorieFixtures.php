<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategorieFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $categories = ['Peinture', 'Dessin', 'Sculpture'];

    	for($i = 0; $i < sizeof($categories); $i++){
		    $category = new Categorie();
            $category->setName($categories[$i]);
            $this->addReference("category$i", $category);
            $category->setSlug(strtolower($categories[$i]));

		    $manager->persist($category);
	    }

        $manager->flush();
    }
}

# M1 Evaluation G2

## Technos
*  PHP 7.3
*  Symfony 5.0.1
*  Webpack 4.41.2
*  MySQL 8

## Démarrer le site web

Installer les dépendances utilisées, lancer la migration pour créer la bdd, et charger les données fictives

*  `composer i`
*  `npm i`
*  `npm run watch`
*  `symfony serve`
*  `symfony console doctrine:migrations:migrate`
*  `symfony console doctrine:fixtures:load -n`

Ne pas oublier de démarrer MySQL.

## Particularité

En raison d'un problème rencontré avec l'utilisation d'*image* pour les fixtures, *imageUrl* a été utilisé.

Il faut donc insérer un lien d'image (ex : 
[https://fr.ubergizmo.com/wp-content/uploads/2012/06/trololo.jpg](https://fr.ubergizmo.com/wp-content/uploads/2012/06/trololo.jpg)) 
plutôt que de parcourir dans le pc.